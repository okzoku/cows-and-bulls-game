from django.contrib import admin
from cows_bulls.models import Game, Guess

# Register your models here.
class GameAdmin(admin.ModelAdmin):
    pass

class GuessAdmin(admin.ModelAdmin):
    pass



admin.site.register(Game, GameAdmin)
admin.site.register(Guess, GuessAdmin)