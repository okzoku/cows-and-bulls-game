# Generated by Django 4.0.4 on 2022-05-17 22:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cows_bulls', '0003_remove_game_box1_remove_game_box2_remove_game_box3_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='box_1_answer',
            field=models.CharField(default='1', max_length=1),
        ),
        migrations.AlterField(
            model_name='game',
            name='box_2_answer',
            field=models.CharField(default='1', max_length=1),
        ),
        migrations.AlterField(
            model_name='game',
            name='box_3_answer',
            field=models.CharField(default='1', max_length=1),
        ),
        migrations.AlterField(
            model_name='game',
            name='box_4_answer',
            field=models.CharField(default='1', max_length=1),
        ),
    ]
